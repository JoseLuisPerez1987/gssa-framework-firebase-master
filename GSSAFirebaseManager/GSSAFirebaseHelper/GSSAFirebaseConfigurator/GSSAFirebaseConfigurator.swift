//
//  GSFirebaseConfigurator.swift
//  GSSAFirebaseManager
//
//  Created by Jose Luis Perez Reyes on 04/05/21.
//

import Foundation
import FirebaseCore
import FirebaseAnalytics
import FirebaseRemoteConfig
import FirebaseMessaging
import FirebaseCrashlytics
import FirebaseDynamicLinks
import FirebaseInstanceID
import FirebaseInstallations
import FirebaseCoreDiagnostics
import FirebasePerformance
import FirebaseABTesting

public final class GSFirebaseConfigurator: NSObject {
    public func initWithFileConfig(nameFileToInitFirebase:String,idBundleGSSAModule:String){
            // Load a named file.
            let filePath = Bundle.main.path(forResource: nameFileToInitFirebase, ofType: "plist")
            guard let fileopts = FirebaseOptions(contentsOfFile: filePath!)
              else { return }
            FirebaseApp.configure(options: fileopts)
        }
}
