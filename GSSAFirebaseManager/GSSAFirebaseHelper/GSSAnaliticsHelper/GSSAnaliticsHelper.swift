//
//  GSSAnaliticsHelper.swift
//  GSSAFirebaseManager
//
//  Created by Jose Luis Perez Reyes on 04/05/21.
//

import Foundation
import FirebaseAnalytics
import FirebaseCore
import FirebasePerformance

public struct GSSAnaliticsHelperScreen {
    var strEvent : String?
    var  strFlow : String?
    var  strScreen : String?
    var  strPreviousScreen : String?
    
    public init() {}
    
    public init( event strEvent : String,flow strFlow : String,screen strScreen : String,previousScreen strPreviousScreen : String?) {
        self.strEvent = strEvent
        self.strFlow = strFlow
        self.strScreen = strScreen
        self.strPreviousScreen  = strPreviousScreen
    }
    public init(flow strFlow : String,screen strScreen : String,previousScreen strPreviousScreen : String?) {
          self.strFlow = strFlow
          self.strScreen = strScreen
          self.strPreviousScreen  = strPreviousScreen
      }
    
}
public struct GSSAnaliticsHelperEvent{
    public var  strEvent : String?
    public var  strFlow : String?
    public var  strScreen : String?
    public var  strInteraction : String?
    public var  strElement:String?
    
    public init() {
    }
    public init(event strEvent : String,flow strFlow : String,screen strScreen : String,interaction strInteraction : String,element strElement:String) {
        self.strEvent = strEvent
        self.strFlow = strFlow
        self.strScreen = strScreen
        self.strInteraction = strInteraction
        self.strElement = strElement
    } 
}
public struct GSSAnaliticsHelperError{
    public var  strEvent : String?
    public var  strFlow : String?
    public var  strScreen : String?
    public var  strPreviousScreen:String?
    public var  strType : String?
    public var  strMessage:String?
    public var  strCode:String?
    public init() {
    }
    public init(event strEvent : String,flow strFlow : String,screen strScreen : String,previousScreen strPreviousScreen:String,type strType : String,message strMessage:String,code strCode:String) {
        self.strEvent =  strEvent
        self.strFlow = strFlow
        self.strScreen = strScreen
        self.strPreviousScreen = strPreviousScreen
        self.strType = strType
        self.strMessage = strMessage
        self.strCode = strCode
    }
}

public class GSSAnalitycs: NSObject {
    
    public class func taggingScreen(event strEvent : String? = NSLocalizedString("description_event_name_view", comment:""),
                             flow strFlow : String?,
                             screen strScreen : String?,
                             previousScreen strPreviousScreen : String?){
                                                          
        guard let strEvent = strEvent , let strFlow = strFlow,  let strScreen = strScreen, let strPreviousScreen = strPreviousScreen else{
            return
        }
        
        var dctParameters : Dictionary<String,Any> = [:]
        dctParameters["flow"] = strFlow
        dctParameters["screen_name"] = strScreen
        dctParameters["previous_screen"] = strPreviousScreen
        Analytics.logEvent(strEvent, parameters: dctParameters)
        
    }
    
    public class func taggingEvent(event strEvent : String? = NSLocalizedString("description_event_name_ui", comment: ""),
                            flow strFlow : String?,
                            screen strScreen : String?,
                            interaction strInteraction : String? = NSLocalizedString("description_interaction_type_click", comment: ""),
                            element strElement:String?){
        guard let strEvent = strEvent, let strFlow = strFlow, let strScreen = strScreen, let strInteraction = strInteraction, let strElement = strElement else{
            return
        }
        
        
        var dctParameters : Dictionary<String,Any> = [:]
        dctParameters["flow"] = strFlow
        dctParameters["screen_name"] = strScreen
        dctParameters["interaction_type"] = strInteraction
        dctParameters["element"] = strElement
        Analytics.logEvent(strEvent, parameters: dctParameters)
    
    }

    public class func taggingErrorSystem(event strEvent : String? = NSLocalizedString("description_error_event_name", comment: ""),
                            flow strFlow : String?,
                            screen strScreen : String?,
                            previousScreen strPreviousScreen:String?,
                            type strType : String? = NSLocalizedString("description_error_type_system", comment: ""),
                            message strMessage:String?,
                            code strCode:String?){
        
        
        guard  let strEvent = strEvent, let strFlow = strFlow, let strScreen = strScreen, let strPreviousScreen = strPreviousScreen, let strType  = strType, let strMessage = strMessage, let strCode = strCode else {
                return
        }
        
        
        var dctParameters : Dictionary<String,Any> = [:]
        dctParameters["flow"] = strFlow
        dctParameters["screen_name"] = strScreen
        dctParameters["previous_screen"] = strPreviousScreen
        dctParameters["type"] = strType
        dctParameters["message"] = strMessage
        dctParameters["code"] = strCode
        Analytics.logEvent(strEvent, parameters: dctParameters)
        
    }
    
    public class func taggingErrorUser(event strEvent : String? = NSLocalizedString("description_error_event_name", comment: ""),
                            flow strFlow : String?,
                            screen strScreen : String?,
                            previousScreen strPreviousScreen:String?,
                            type strType : String? = NSLocalizedString("description_error_type_user", comment: ""),
                            message strMessage:String?,
                            code strCode:String?){
        
        
        guard  let strEvent = strEvent, let strFlow = strFlow, let strScreen = strScreen, let strPreviousScreen = strPreviousScreen, let strType  = strType, let strMessage = strMessage, let strCode = strCode else {
                return
        }
        
        
        var dctParameters : Dictionary<String,Any> = [:]
        dctParameters["flow"] = strFlow
        dctParameters["screen_name"] = strScreen
        dctParameters["previous_screen"] = strPreviousScreen
        dctParameters["type"] = strType
        dctParameters["message"] = strMessage
        dctParameters["code"] = strCode
        Analytics.logEvent(strEvent, parameters: dctParameters)
        
    }
    
    public class func taggingOperation(event strEvent : String? = "chambeador_success",
                              flow strFlow : String?,
                              screen strScreen : String?,
                              operation strOperation : String?){
                                                           
         guard let strEvent = strEvent , let strFlow = strFlow,  let strScreen = strScreen, let strOperation = strOperation else{
             return
         }
         
         var dctParameters : Dictionary<String,Any> = [:]
         dctParameters["flow"] = strFlow
         dctParameters["screen_name"] = strScreen
         dctParameters["operation"] = strOperation
         Analytics.logEvent(strEvent, parameters: dctParameters)
         
     }
     
    public  class func taggingOperationCustom (event strEvent : String? = "name_module_success",dctParameters:[String:String]?,typeModule:String){
        guard let strEvent = strEvent , let dctParameters = dctParameters else{
                    return
                }
        Analytics.logEvent(strEvent, parameters: dctParameters)
    }
    public  class func taggingSetUserID(strUserId: String?, strGender:String? = nil){
        if let user = strUserId{
            Analytics.setUserID(user)
            Analytics.setUserProperty(user, forName: "user_id")
        }
    }
    
}



